

function layThongTinTuForm() {
    id = document.getElementById("idSp").value.trim();
    ten = document.getElementById("TenSP").value.trim();
    gia = document.getElementById("GiaSP").value.trim();
    hinhAnh = document.getElementById("HinhSP").value.trim();
    mota = document.getElementById("MoTa").value.trim();

    var sp = new SanPham(id, ten, gia, hinhAnh, mota)
    return sp;
}
function renderDssp(list) {
    var contentHTML = "";
    // contentHTML chuổi chứa các thẻ tr sau này sẽ innerHTML vào thẻ tbody

    for (var i = 0; i < list.length; i++) {

        var currentSv = list[i];
        var contentTr = ` <tr>
        <td>${currentSv.id}</td>
        <td>${currentSv.name} </td>
        <td>${currentSv.price} </td>
        <td>
        <img src="${currentSv.img}" width="100px">
        </td>
       <td>${currentSv.desc} </td>
       <td>
       <button onclick="layThongTin(${currentSv.id})" class="btn btn-primary" >sữa</button>
       <button onclick="xoaSanPham(${currentSv.id})" class="btn btn-danger" >xóa</button>
       </td>
        </tr>`;

        contentHTML += contentTr
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML
}

function showThongTinLenForm(sp) {
    document.getElementById("idSp").value = sp.id;
    document.getElementById("TenSP").value = sp.name;
    document.getElementById("GiaSP").value = sp.price;
    document.getElementById("HinhSP").value = sp.img;
    document.getElementById("MoTa").value = sp.desc;
}
function resetForm() {
    // reset form
    document.getElementById("formSp").reset();
}
