function kiemTraRong(value, idError) {
    if (value.length == 0) {
        document.getElementById(idError).innerText = "trường này ko để rổng";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function kiemTraTen(value, idError) {
    const result = /^[a-z\s]+$/i
    var isName = result.test(value)
    if (isName) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "tên này ko hợp lệ";
        return false;
    }
}
function kiemTraChuSo(value, idError) {
    const result = /^\d+$/;
    var isName = result.test(value)
    if (isName) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = 'block'
        document.getElementById(idError).innerText = "giá không hợp lệ";
        return false;
    }
}
