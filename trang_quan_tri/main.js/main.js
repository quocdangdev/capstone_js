
const BASE_URL = "https://633ec0700dbc3309f3bc569f.mockapi.io"
var fetchProducts = function () {
    axios({
        url: `${BASE_URL}/products`,
        method: "GET",
    })
        .then(function (response) {
            renderPhone(response.data);
        })
        .catch(function (error) {
            console.log("🚀 error", error);
        });
}
fetchProducts()
var renderPhone = function (list) {
    var contentHTML = "";
    list.forEach(function (phone) {
        contentHTML += `
       <tr>
       <td>${phone.id}</td>
       <td>${phone.name} </td>
       <td>${phone.price}$ </td>
       <td>
       <img src="${phone.img}" width="100px">
       </td>
      <td>${phone.desc} </td>
      <td>
      <button data-toggle="modal"
      data-target="#myModal" onclick="ThongTinChiTiet(${phone.id})" class="btn btn-primary" >sữa</button>
      <button onclick="xoaSanPham(${phone.id})" class="btn btn-danger" >xóa</button>
      </td>
       </tr>
        `;
    });
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

var xoaSanPham = function (id) {
    axios({
        url: `${BASE_URL}/products/${id}`,
        method: "DELETE",
    })
        .then(function (res) {
            console.log("res", res);
            fetchProducts();
        })
        .catch(function (err) {
            console.log("err", err)
        })
}
// thêm
var addProducts = function () {
    var sp = layThongTinTuForm();
    var isValid = true;
    isValid = isValid & kiemTraRong(sp.id, "spanId")
    isValid = isValid & kiemTraRong(sp.name, "spanTenSp") && kiemTraTen(sp.name, "spanTenSp")
    isValid = isValid & kiemTraRong(sp.name, "spanGiaSp") && kiemTraChuSo(sp.price, "spanGiaSp")
    isValid = isValid & kiemTraRong(sp.name, "spanHinhSp")
    isValid = isValid & kiemTraRong(sp.name, "spanMota") && kiemTraTen(sp.desc, "spanMota")

    console.log("sv: ", sp);
    if (isValid) {
        axios({
            url: `${BASE_URL}/products`,
            method: "POST",
            data: sp,
        })
            .then(function (res) {
                console.log("them thành công")
                fetchProducts();
                resetForm();
            })
            .catch(function (err) {
                console.log("loc")
            });
    }
}
var ThongTinChiTiet = function (id) {
    axios({
        url: `${BASE_URL}/products/${id}`,
        method: 'PUT',
    })
        .then(function (res) {
            console.log(res)
            showThongTinLenForm(res.data)
        })
        .catch(function (err) {
            console.log(err)
        })
}
var capNhap = function () {
    var sp = layThongTinTuForm();
    console.log("🚀 ~ file: main.js ~ line 85 ~ capNhap ~ sp", sp)
    axios({
        url: `${BASE_URL}/products/${sp.id}`,
        method: "PUT",
        data: sp,
    })
        .then(function (res) {
            fetchProducts();
        })
        .catch(function (err) {
            console.log("err", err)
        })
    resetForm();
}