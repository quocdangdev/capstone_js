const BASE_URL = "https://633ec0700dbc3309f3bc569f.mockapi.io"
var fetchProducts = function () {
    axios({
        url: `${BASE_URL}/products`,
        method: "GET",
    })
        .then(function (response) {
            renderPhone(response.data);
        })
        .catch(function (error) {
            console.log("🚀 error", error);
        });
}
fetchProducts()
var renderPhone = function (list) {
    var contentHTML = "";
    list.forEach(function (phone) {
        contentHTML += `
        <div class="col-3">
        <div class="card">
  <img src="${phone.img}" class="card-img-top">
  <div class="card-body">
    <h5 class="card-title">${phone.name}</h5>
    <p> ${phone.price} </p>
    <p> ${phone.type} </p>
    <a href="#" class="btn btn-primary">Buy</a>
  </div>
</div>
        </div>`;
    });
    document.getElementById("tbodyPhone").innerHTML = contentHTML;
};

// console.log("🚀 ~ file: main.js ~ line 37 ~ renderPhone", renderPhone)


// var renderCheckPhone = function () {
//     fetchProducts.map(function (phone) {
//         if (phone.type == 'samsung') {
//             return 'che'
//         }
//     })
// }
// renderCheckPhone();


// function check() {
//     let loai = document.getElementById("tbodyPhone");
//     if (loai == 'samsung') {
//         renderCheckPhone();
//     }
// }

